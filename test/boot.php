<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . "/env.php";

use FullCycle\SendgridApi\SendgridApiConfig;

$apiAccessKey = env("SENDGRID_ACCESS_KEY");
SendgridApiConfig::setApiAccessKey($apiAccessKey);



