<?php 

namespace FullCycle\SendgridApi;

use FullCycle\SendgridApi\SendgridApiResource;
use FullCycle\ApiFramework\Util\Util;

/**
 * @author thrasher
 * 
 * @example
 * 
 */

class SendgridCategories extends SendgridApiResource {
    protected $_request_url="categories";
    protected $_method = "GET";
    
    function __construct($id = null, $opts = null) {
	parent::__construct($id,$opts);
    }

   function makeUri() {
	$uri=parent::makeUri();
	return $uri;
   }
 
}

