<?php

namespace FullCycle\SendgridApi;

use FullCycle\ApiFramework\ApiResource;
use Carbon\Carbon;
use FullCycle\SendgridApi\SendgridApiConfig;

class SendgridApiResource extends ApiResource {
    protected $_apiConfigClass=SendgridApiConfig::class;


    function __construct($id = null, $opts = null) {
        parent::__construct($id,$opts);
    }

    
    function initHeader(array $options=[]) {
    }

    function getVersion() {
	return $this->_apiConfigClass::getVersion();
    }

    function getApiAccessKey() {
	return $this->_apiConfigClass::getApiAccessKey();
    }

    function makeUri() {
        $uri = "{$this->getApiBaseUrl()}/{$this->getVersion()}/{$this->getRequestUrl()}";
        return $uri;
    }
    
    function mergeRetrieveOptions($options) {
        $this->_retrieveOptions=array_merge($this->_retrieveOptions,$options);
    }
    
    function optionAdjust() {
        if (!empty($this->_opts['options'])) {
            $this->mergeRetrieveOptions($this->_opts['options']);
        }
    }
    
    function json_decode($json) {
	return parent::json_decode($json);
    }

    function getHeaders() {
	$apiKey = $this->getApiAccessKey();
	$defaultHeaders = [
            'Authorization' => "Bearer {$apiKey}",
        ];
        return $defaultHeaders;


    }

}


