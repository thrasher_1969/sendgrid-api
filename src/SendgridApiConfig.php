<?php

namespace FullCycle\SendgridApi;

use FullCycle\ApiFramework\ApiConfigClass;
use \Exception;

class SendgridApiConfig extends ApiConfigClass {
	static protected $apiBase="https://api.sendgrid.com";
	static protected $apiProductionBase = "https://api.sendgrid.com";
	static protected $apiSandboxBase = "https://api.sendgrid.com";
	static protected $apiVersion = "v3";
	
	static function setProduction() {
	    static::$apiBase = static::$apiProductionBase;
	}
	
	static function setSandbox() {
	    static::$apiBase = static::$apiSandboxBase;
	}

	static function getMerchantId() {
		return static::$merchantId;
	}
	
	static function getVersion() {
	    return static::$apiVersion;
	}
}

